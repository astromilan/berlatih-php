<?php
function ubah_huruf($string){
//kode di sini
	$tampung = "";
	$abjad = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];

	echo "Kata " . $string . '<br>';
	for ($i = 0 ; $i < strlen($string); $i++){
		for ($j = 0 ; $j < count($abjad); $j++) {
			if ( $abjad[$j] == $string[$i] ) {
				echo $abjad[$j].' diubah menjadi '.$abjad[$j+1].'<br>';
				$tampung .= $abjad[$j+1];
			}
		}

	}
	echo "Menjadi kata  ".$tampung.'<hr><br>';
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>